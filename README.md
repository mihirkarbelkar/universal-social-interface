Step 1: 
	git clone https://fauxwizard@bitbucket.org/fauxwizard/universal-social-interface.git usi
	
Step 2:
	create a virtual env 

Step 3:
	run pip3 install -r req.txt

Step 4:
	change directory to prac

Step 5:
	run "python3 manage.py runserver" on unix
	run "py manage.py runserver" on windows
 